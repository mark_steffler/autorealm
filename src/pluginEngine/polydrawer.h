#ifndef POLYDRAWER_H
#define POLYDRAWER_H

#include "drawer.h"

class wxMenu;

class PolyDrawer : public Drawer
{
	static ID m_menuIds[3];///\todo move in gui module instead of pluginEngine
	static wxMenu *m_menu; ///\todo move in gui module instead of pluginEngine

	bool m_useShift=false;/// use shift+right click to create closed shapes, right click to create opened one.
public:
	PolyDrawer(RenderWindow *window, std::unique_ptr<Renderer> r, Render::TagList const &tags);
	PolyDrawer(PolyDrawer const& other);
	virtual ~PolyDrawer(void)throw();

	void installEventManager(void) throw() override;
	void removeEventManager(void) throw() override;
	void finalizeShape(wxCommandEvent &event);
	void contextMenu(wxContextMenuEvent &event);
	void firstPoint(wxMouseEvent &event);
	void secondPoint(wxMouseEvent &event);
	void switchShift(wxCommandEvent &event);

	void bindMouse(void);
	void unbindMouse(void);
};

#endif
