#ifndef SINGLEDRAWER_H
#define SINGLEDRAWER_H

#include "drawer.h"

class SingleDrawer : public Drawer
{
public:
	SingleDrawer(RenderWindow *window, std::unique_ptr<Renderer> r, Render::TagList const &tags);
	SingleDrawer(SingleDrawer const& other);
	virtual ~SingleDrawer(void)throw();

	void installEventManager(void) throw() override;
	void removeEventManager(void) throw() override;
	void finalizeShape(wxMouseEvent &event);
	void firstPoint(wxMouseEvent &event);
	void secondPoint(wxMouseEvent &event);
};

#endif
