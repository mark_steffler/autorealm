/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#ifndef TOOLBARITEMCONFIG_H
#define TOOLBARITEMCONFIG_H

#include "itemconfig.h"

namespace fs=boost::filesystem;
namespace po=boost::program_options;

class ToolbarItemConfig: public ItemConfig
{
	std::string m_label;
	std::string m_bmp;

public:
	/**
	 * \brief retrieve informations to build a toolbarItem from a configuration file
	 * It have same \pre as ItemConfig's version plus:
	 * \throw runtime_error if the configuration contains an invalid path
	 * \pre if a bitmap section is found, it must link to an existing file
	 */
	ToolbarItemConfig(fs::path const& rootDir);
	ToolbarItemConfig(ToolbarItemConfig && other);
	ToolbarItemConfig(ToolbarItemConfig const& other);
	std::string label(void)const;/// label getter
	std::string bmp(void)const;///bitmap filename getter
};

#include <wx/aui/auibar.h>
#include <wx/aui/framemanager.h>

wxAuiPaneInfo buildPaneInfo(Leaf<ToolbarItemConfig> &data);

#endif
