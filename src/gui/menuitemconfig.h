/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#ifndef MENUITEMCONFIG_H
#define MENUITEMCONFIG_H

#include "itemconfig.h"

namespace fs=boost::filesystem;
namespace po=boost::program_options;

class MenuItemConfig: public ItemConfig
{
	std::string m_text;
	bool m_showTitle=false;

public:
	MenuItemConfig(fs::path const& rootDir);
	MenuItemConfig(MenuItemConfig && other);
	MenuItemConfig(MenuItemConfig const& other);
	std::string text(void)const;
	bool showTitle(void)const;
};

wxMenuBar* createMenuFromFolder(Node<MenuItemConfig> & origin);
wxMenu* createMenu(Node<MenuItemConfig> & origin);

#endif
