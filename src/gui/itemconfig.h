/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#ifndef ITEMCONFIG_H
#define ITEMCONFIG_H

#include <string>
#include <stdio.h>
#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "wxadapter.h"
#include <wx/menu.h>
#include <tree.h>

namespace fs=boost::filesystem;
namespace po=boost::program_options;

class ItemConfig
{
protected:
	std::string m_desc;
	std::string m_plugin;
	ItemKind m_kind=NORMAL;
	uint16_t m_id;

protected:
	po::options_description m_odFile;
	fs::path m_path;

public:
	/**
	 * \brief retrieve informations to build a toolbarItem from a configuration file
	 * \throw runtime_error if configuration file is not found
	 * \pre rootDir must be an existing file or folder
	 * \pre if rootDir is a folder, it must contain a file with the same name
	 */
	ItemConfig(fs::path const& rootDir);
	ItemConfig(ItemConfig && other);
	ItemConfig(ItemConfig const& other);
	std::string desc(void)const;///description getter
	std::string plugin(void)const;///plug-in name (to link the item with) getter
	ItemKind kind(void)const;/// kind getter (checkbox, list, ...)
	///\todo improve documentation for kind
	uint16_t id(void)const;/// id getter
	void setId(uint16_t id);/// is setter
};

#endif
