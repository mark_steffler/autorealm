FILE(GLOB source_files "*.cpp")

ADD_LIBRARY(save SHARED ${source_files} )
TARGET_LINK_LIBRARIES(save pluginengine renderengine ${wxWidgets_LIBRARIES} ${Pluma_LIBRARY} ${Boost_SERIALIZATION_LIBRARY})
